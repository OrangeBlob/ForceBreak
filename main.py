import tkinter as tk
from tkinter import ttk, messagebox
from notifypy import Notify


class ForceBreak:
    def __init__(self, focus_time, break_time, focus_window, focus_label):
        try:
            self.focus_time = int(focus_time.get())
            self.break_time = int(break_time.get())
        except ValueError:
            messagebox.showerror("Error", "Your input is invalid.")
            return

        self.focus_window = focus_window
        self.focus_label = focus_label

        self.break_count = None
        self.focus_count = None

        self.running = True

    def start_timer(self):
        self.running = True
        self.focus_timer()

    def stop_timer(self):
        self.running = False
        self.focus_label.config(text="Stopped")
        self.focus_window.update()

    def focus_timer(self):
        self.focus_count = self.focus_time
        self._focus_timer()

    def _focus_timer(self):
        if self.focus_count > 0 and self.running:
            if self.focus_count == 1:
                notification = Notify()
                notification.title = "ForceBreak"
                notification.urgency = "critical"
                notification.message = "1 minute left until next break. Please finish your work."
                notification.send()
            self.focus_label.config(text=str(self.focus_count) + " min.")
            self.focus_window.update()
            self.focus_count -= 1
            self.focus_window.after(60000, self._focus_timer)
        else:
            if self.running:
                self.break_timer()

    def break_timer(self):
        break_window = tk.Tk()
        break_window.attributes('-fullscreen', True)
        break_window.attributes('-topmost', True)
        break_window.title("")
        break_window['bg'] = '#FFFBE4'

        count_label = tk.Label(break_window, text="",
                               font=("", 20), bg='#FFFBE4')
        count_label.place(relx=0.5, rely=0.6, anchor="center")

        label = tk.Label(break_window, text="It's time to take a break!",
                         font=("", 36, "bold"), bg='#FFFBE4')
        label.place(relx=0.5, rely=0.5, anchor="center")

        self.break_count = self.break_time
        self._break_timer(break_window, count_label)

    def _break_timer(self, break_window, count_label):
        if self.break_count > 0 and self.running:
            count_label.config(text=f"{str(self.break_count)} min. left")
            break_window.update()
            self.break_count -= 1
            break_window.after(60000, self._break_timer, break_window, count_label)
        else:
            break_window.destroy()
            if self.running:
                self.focus_timer()


def ui():
    def start_timer(fe, be, iw, cl):
        global force_break
        force_break = ForceBreak(fe, be, iw, cl)
        force_break.start_timer()

    def stop_timer():
        if force_break:
            force_break.stop_timer()

    # Define the window
    input_window = tk.Tk()
    input_window.title("ForceBreak")
    input_window.resizable(width=False, height=False)
    input_window.geometry("650x300")

    # Apply themes and colors
    style = ttk.Style()
    style.theme_use("clam")
    input_window.configure(background="#dcdad5")

    # Create input frame
    frame = ttk.Frame(input_window, relief="solid", borderwidth=2)
    frame.place(x=75, y=55)
    frame.pack_propagate(False)

    # Create counting frames
    count_frame = ttk.Frame(input_window, relief="solid", borderwidth=0, width=300, height=300)
    count_frame.place(x=310, y=0)

    # Create label amd entry fields
    frame.grid_rowconfigure(0, minsize=20)

    focus_label = ttk.Label(frame, text="Focus time:")
    focus_label.grid(row=1, column=0, padx=30, pady=0, sticky='w')

    focus_entry = ttk.Entry(frame)
    focus_entry.insert(0, "40")
    focus_entry.grid(row=2, column=0, padx=30, pady=0)

    frame.grid_rowconfigure(3, minsize=10)

    break_label = ttk.Label(frame, text="Break time:")
    break_label.grid(row=4, column=0, padx=30, pady=0, sticky='w')

    break_entry = ttk.Entry(frame)
    break_entry.insert(0, "10")
    break_entry.grid(row=5, column=0, padx=30, pady=0)

    frame.grid_rowconfigure(6, minsize=20)

    # Create counting labels
    timeleft_label = ttk.Label(count_frame, text="Time left:")
    timeleft_label.place(relx=0.5, rely=0.25, anchor="center")

    count_label = ttk.Label(count_frame, text="Stopped", font=("", 25))
    count_label.place(relx=0.5, rely=0.43, anchor="center")

    # Create buttons
    ok_button = ttk.Button(frame, text="Start",
                           command=lambda: start_timer(focus_entry, break_entry, input_window, count_label))
    ok_button.grid(row=7, column=0, padx=30, pady=0)

    stop_button = ttk.Button(count_frame, text="⏹︎", command=lambda: stop_timer(), width=3)
    stop_button.place(relx=0.5, rely=0.67, anchor="center")

    frame.grid_rowconfigure(9, minsize=20)

    return input_window


force_break = None
ui().mainloop()
